# We've got opencm3 in the folder with us
BUILD_DIR 	:= ./build
OPENCM3_DIR := ./libopencm3
FREERTOS_DIR	:= ./freertos
INC_DIR		:= ./inc

# Our output name
TARGET = main

# Linker script for our MCU
LDSCRIPT = stm32f1.ld

# Using the stm32f1 series chip
LIBNAME		= opencm3_stm32f1
DEFS		+= -DSTM32F1 -DSTM32F103xB

# Target-specific flags
# FP_FLAGS	?= -msoft-float
ARCH_FLAGS	= -mthumb -mcpu=cortex-m3 $(FP_FLAGS)

# Where our Black Magic Probe is attached
BMP_PORT = /dev/ttyACM0

## Boilerplate

# Compiler configuration
PREFIX		?= arm-none-eabi
CC		:= $(PREFIX)-gcc
CXX		:= $(PREFIX)-g++
LD		:= $(PREFIX)-gcc
AR		:= $(PREFIX)-ar
AS		:= $(PREFIX)-as
SIZE		:= $(PREFIX)-size
OBJCOPY		:= $(PREFIX)-objcopy
OBJDUMP		:= $(PREFIX)-objdump
GDB		:= $(shell which $(PREFIX)-gdb)
STFLASH		= $(shell which st-flash)
OPT		:= -Os
DEBUG		:= -ggdb3
CSTD		?= -std=c11

# C flags
TGT_CFLAGS	+= $(OPT) $(CSTD) $(DEBUG)
TGT_CFLAGS	+= $(ARCH_FLAGS)
TGT_CFLAGS	+= -Wextra -Wshadow -Wimplicit-function-declaration
TGT_CFLAGS	+= -Wredundant-decls -Wmissing-prototypes -Wstrict-prototypes
TGT_CFLAGS	+= -fno-common -ffunction-sections -fdata-sections

# C++ flags
TGT_CXXFLAGS	+= $(OPT) $(CXXSTD) $(DEBUG)
TGT_CXXFLAGS	+= $(ARCH_FLAGS)
TGT_CXXFLAGS	+= -Wextra -Wshadow -Wredundant-decls -Weffc++
TGT_CXXFLAGS	+= -fno-common -ffunction-sections -fdata-sections
TGT_CXXFLAGS	+= -std=c++17

# C & C++ preprocessor common flags
TGT_CPPFLAGS	+= -MD -MF"$(@:.o=.d)"
TGT_CPPFLAGS	+= -Wall -Wundef
TGT_CPPFLAGS	+= $(DEFS)

# Linker flags
TGT_LDFLAGS		+= --static -nostartfiles
TGT_LDFLAGS		+= -T$(LDSCRIPT)
TGT_LDFLAGS		+= $(ARCH_FLAGS) $(DEBUG)
TGT_LDFLAGS		+= -Wl,-Map=$(*).map -Wl,--cref
TGT_LDFLAGS		+= -Wl,--gc-sections
ifeq ($(V),99)
TGT_LDFLAGS		+= -Wl,--print-gc-sections
endif

# Used libraries
C_SOURCES	= main.c $(FREERTOS_DIR)/heap_4.c $(FREERTOS_DIR)/list.c $(FREERTOS_DIR)/port.c $(FREERTOS_DIR)/tasks.c $(FREERTOS_DIR)/opencm3.c
OBJECTS 	= $(addprefix $(BUILD_DIR)/,$(notdir $(C_SOURCES:.c=.o)))
LIBOPENCM3 = $(LIBPATHS)/lib/libopencm3_stm32f1.a

vpath %.c $(sort $(dir $(C_SOURCES)))
vpath %.o,%d $(BUILD_DIR)

DEFS		+= -I$(OPENCM3_DIR)/include -I$(FREERTOS_DIR)/ -I$(INC_DIR)
LDFLAGS		+= -L$(OPENCM3_DIR)/lib
LDLIBS		+= -l$(LIBNAME)
LDLIBS		+= -Wl,--start-group -lc -lgcc -lnosys -Wl,--end-group

.SUFFIXES: .elf .bin .hex .srec .list .map .images
.SECONDEXPANSION:
.SECONDARY:

all: elf size
obj: $(OBJECTS)
size: $(TARGET).size
elf: $(TARGET).elf
bin: $(TARGET).bin
hex: $(TARGET).hex
srec: $(TARGET).srec
list: $(TARGET).list
flash: $(TARGET).flash

GENERATED_BINARIES=$(TARGET).elf $(TARGET).bin $(TARGET).hex $(TARGET).srec $(TARGET).list $(TARGET).map

%.bin: %.elf
	$(OBJCOPY) -OTARGET $(*).elf $(*).bin

%.hex: %.elf
	$(OBJCOPY) -Oihex $(*).elf $(*).hex

%.srec: %.elf
	$(OBJCOPY) -Osrec $(*).elf $(*).srec

%.list: %.elf
	$(OBJDUMP) -S $(*).elf > $(*).list

%.elf %.map: $(OBJECTS) $(LDSCRIPT)
	$(LD) $(TGT_LDFLAGS) $(LDFLAGS) $(OBJECTS) $(LDLIBS) -o $(TARGET).elf

$(BUILD_DIR)/%.o: %.c $(LIBOPENCM3) | $(BUILD_DIR)
	$(CC) $(TGT_CFLAGS) -c $(CFLAGS) $(TGT_CPPFLAGS) $(CPPFLAGS) $< -o $@

%.o: %.cxx
	$(CXX) $(TGT_CXXFLAGS) $(CXXFLAGS) $(TGT_CPPFLAGS) $(CPPFLAGS) -o $(*).o -c $(*).cxx

%.o: %.cpp
	$(CXX) $(TGT_CXXFLAGS) $(CXXFLAGS) $(TGT_CPPFLAGS) $(CPPFLAGS) -o $(*).o -c $(*).cpp


$(LIBOPENCM3):
	make -C libopencm3 TARGETS=stm32/f1 $(MAKEFLAGS)

$(BUILD_DIR):
	mkdir $@

%.size: %.elf
	@echo "Output code size:"
	@$(SIZE) -A -d $(*).elf | egrep 'text|data|bss' | awk ' \
    function human(x) { \
        if (x<1000) {return x} else {x/=1024} \
        s="kMGTEPZY"; \
        while (x>=1000 && length(s)>1) \
            {x/=1024; s=substr(s,2)} \
        return int(x+0.5) substr(s,1,1) \
    } \
	{printf("%10s %8s\n", $$1, human($$2))} \
'

%.flash: %.hex
	$(STFLASH) --format ihex write $(TARGET).hex

clean:
	$(RM) $(GENERATED_BINARIES) generated.* $(OBJECTS) $(OBJECTS:%.o=%.d)

debug:
	$(GDB) $(TARGET).elf

.PHONY: images clean stylecheck styleclean elf bin hex srec list
-include $(OBJECTS:.o=.d)