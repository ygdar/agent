cmake_minimum_required(VERSION 3.13)

set(FREERTOS_SOURCES
    port.c
    heap_4.c
    opencm3.c

    tasks.c
    list.c
    queue.c
)

set(FREERTOS_SOURCES ${FREERTOS_SOURCES} PARENT_SCOPE)
message(${FREERTOS_SOURCES})
