//
// Created by zombi on 10/22/19.
//

//#include <stdio.h>

#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/usb/cdc.h>
#include <libopencm3/usb/usbd.h>

#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>

#include "inc/usbcdc.h"


static volatile int initialized = 0;
static QueueHandle_t usb_txq;
static QueueHandle_t usb_rxq;

static const struct usb_device_descriptor dev = {
	.bLength = USB_DT_DEVICE_SIZE,
	.bDescriptorType = USB_DT_DEVICE,
	.bcdUSB = 0x0200,
	.bDeviceClass = USB_CLASS_CDC,
	.bDeviceSubClass = 0,
	.bDeviceProtocol = 0,
	.bMaxPacketSize0 = 64, // 64
	.idVendor = 0x0483, // STMicroelectronics
	.idProduct = 0x5740, // Virtual COM Port
	.bcdDevice = 0x0200,
	.iManufacturer = 1,
	.iProduct = 2,
	.iSerialNumber = 3,
	.bNumConfigurations = 1,
};

static const struct usb_endpoint_descriptor ecommunicate[] = {
	{
		.bLength = USB_DT_ENDPOINT_SIZE,
		.bDescriptorType = USB_DT_ENDPOINT,
		.bEndpointAddress = ENDPOINT_COMM_ADDR_IN,
		.bmAttributes = USB_ENDPOINT_ATTR_INTERRUPT,
		.wMaxPacketSize = 16,
		.bInterval = 255,
	}
};

static const struct usb_endpoint_descriptor edata[] = {
	{
		.bLength = USB_DT_ENDPOINT_SIZE,
		.bDescriptorType = USB_DT_ENDPOINT,
		.bEndpointAddress = ENDPOINT_DATA_ADDR_OUT,
		.bmAttributes = USB_ENDPOINT_ATTR_BULK,
		.wMaxPacketSize = 64,
		.bInterval = 1,
	},
	{
		.bLength = USB_DT_ENDPOINT_SIZE,
		.bDescriptorType = USB_DT_ENDPOINT,
		.bEndpointAddress = ENDPOINT_DATA_ADDR_IN,
		.bmAttributes = USB_ENDPOINT_ATTR_BULK,
		.wMaxPacketSize = 64,
		.bInterval = 1
	}
};

static const struct {
	struct usb_cdc_header_descriptor header;
	struct usb_cdc_call_management_descriptor call_mgmt;
	struct usb_cdc_acm_descriptor acm;
	struct usb_cdc_union_descriptor cdc_union;
} __attribute((packed)) cdcacm_functional_descriptors = {
	.header = {
		.bFunctionLength = sizeof(struct usb_cdc_header_descriptor),
		.bDescriptorType = CS_INTERFACE,
		.bDescriptorSubtype = USB_CDC_TYPE_HEADER,
		.bcdCDC = 0x0110
	},
	.call_mgmt = {
		.bFunctionLength = sizeof(struct usb_cdc_call_management_descriptor),
		.bDescriptorType = CS_INTERFACE,
		.bDescriptorSubtype = USB_CDC_TYPE_CALL_MANAGEMENT,
		.bmCapabilities = 0,
		.bDataInterface = 1
	},
	.acm = {
		.bFunctionLength = sizeof(struct usb_cdc_acm_descriptor),
		.bDescriptorType = CS_INTERFACE,
		.bDescriptorSubtype = USB_CDC_TYPE_ACM,
		.bmCapabilities = 0,
	},
	.cdc_union = {
		.bFunctionLength = sizeof(struct usb_cdc_union_descriptor),
		.bDescriptorType = CS_INTERFACE,
		.bDescriptorSubtype = USB_CDC_TYPE_UNION,
		.bControlInterface = 0,
		.bSubordinateInterface0 = 1
	}
};

static const struct usb_interface_descriptor icommunicate[] = {
	{
		.bLength = USB_DT_INTERFACE_SIZE,
		.bDescriptorType = USB_DT_INTERFACE,
		.bInterfaceNumber = 0,
		.bAlternateSetting = 0,
		.bNumEndpoints = 1,
		.bInterfaceClass = USB_CLASS_CDC,
		.bInterfaceSubClass = USB_CDC_SUBCLASS_ACM,
		.bInterfaceProtocol = USB_CDC_PROTOCOL_AT,
		.iInterface = 0,

		.endpoint = ecommunicate,

		.extra = &cdcacm_functional_descriptors,
		.extralen = sizeof(cdcacm_functional_descriptors),
	}
};

static const struct usb_interface_descriptor idata[] = {
	{
		.bLength = USB_DT_INTERFACE_SIZE,
		.bDescriptorType = USB_DT_INTERFACE,
		.bInterfaceNumber = 1,
		.bAlternateSetting = 0,
		.bNumEndpoints = 2,
		.bInterfaceClass = USB_CLASS_DATA,
		.bInterfaceSubClass = 0,
		.bInterfaceProtocol = 0,
		.iInterface = 0,

		.endpoint = edata,
	}
};

static const struct usb_interface ifaces[] = {
	{
		.num_altsetting = 1,
		.altsetting = icommunicate
	},
	{
		.num_altsetting = 1,
		.altsetting = idata
	}
};

static const struct usb_config_descriptor config = {
	.bLength = USB_DT_CONFIGURATION_SIZE,
	.bDescriptorType = USB_DT_CONFIGURATION,
	.wTotalLength = 0,
	.bNumInterfaces = 2,
	.bConfigurationValue = 1,
	.iConfiguration = 0,
	.bmAttributes = 0x80,
	.bMaxPower = 0x32,

	.interface = ifaces,
};

static const char * usb_strings[] = {
	"libusbcdc.c driver",
	"CDC-ACM module",
	"WGDEMO",
};

uint8_t usbd_control_buffer[128];

static enum usbd_request_return_codes cdcacm_control_request(
	usbd_device* udev,
	struct usb_setup_data* req,
	uint8_t** buf,
	uint16_t* len,
	void (** complete) (usbd_device* udev, struct usb_setup_data* req)
	) {
	switch (req->bRequest) {
		case USB_CDC_REQ_SET_CONTROL_LINE_STATE: {
			return USBD_REQ_HANDLED;
		}
		case USB_CDC_REQ_SET_LINE_CODING: {
			if (*len < sizeof(struct usb_cdc_line_coding))
				return USBD_REQ_NOTSUPP;

			return USBD_REQ_HANDLED;
		}
	}

	return USBD_REQ_NOTSUPP;
}

static void cdcacm_data_rx_cb(usbd_device* udev, uint8_t ep) {
	uint16_t rx_avail = uxQueueSpacesAvailable(usb_rxq);
	uint8_t buff[64];
	uint8_t log[64];

	if (rx_avail < 0)
		return;

	uint16_t len_to_read = sizeof(buff) < rx_avail ? sizeof(buff) : rx_avail;
	uint16_t packet_to_read = usbd_ep_read_packet(udev, ENDPOINT_DATA_ADDR_OUT, buff, 64);

//	sprintf((char*)log, "len_to_read: %d", rx_avail);

//	usb_putc(ep);
	usb_puts(buff);

	for (uint16_t i = 0; i < len_to_read; i++)
		xQueueSendToBack(usb_rxq, &buff[i], 0);
}

static void cdcacm_set_config(usbd_device* udev, uint16_t value) {
	usbd_ep_setup(udev, ENDPOINT_DATA_ADDR_OUT, USB_ENDPOINT_ATTR_BULK, 64, cdcacm_data_rx_cb);
	usbd_ep_setup(udev, ENDPOINT_DATA_ADDR_IN, USB_ENDPOINT_ATTR_BULK, 64, NULL);
	usbd_ep_setup(udev, ENDPOINT_COMM_ADDR_IN, USB_ENDPOINT_ATTR_INTERRUPT, 16, NULL);

	usbd_register_control_callback(
		udev,
		USB_REQ_TYPE_CLASS | USB_REQ_TYPE_INTERFACE,
		USB_REQ_TYPE_TYPE | USB_REQ_TYPE_RECIPIENT,
		cdcacm_control_request);

	initialized = 1;
}

static void usb_task(void* arg) {
	usbd_device* udev = (usbd_device*) arg;
	char txbuf[64];
	uint16_t txlen = 0;

	for (;;) {
		usbd_poll(udev);
		if (usb_ready()) {
			while (txlen < sizeof(txbuf) && xQueueReceive(usb_txq, &txbuf[txlen], 0) == pdPASS)
				++txlen;
			if (txlen > 0) {
				if (usbd_ep_write_packet(udev, ENDPOINT_DATA_ADDR_IN, txbuf, txlen) != 0)
					txlen = 0;
			} else {
				taskYIELD();
			}
		} else {
			taskYIELD();
		}
	}
}

void usb_putc(uint8_t ch) {
	static const uint8_t br = '\r';
	while (!usb_ready()) {
		taskYIELD();
	}

	if (ch == '\n')
		xQueueSend(usb_txq, &br, portMAX_DELAY);
	xQueueSend(usb_txq, &ch, portMAX_DELAY);
}

void usb_puts(const uint8_t* buf) {
	while (*buf)
		usb_putc(*buf++);
}

void usb_start(uint32_t priority) {
	usb_txq = xQueueCreate(128, sizeof(char));
	usb_rxq = xQueueCreate(128, sizeof(char));

	rcc_periph_clock_enable(RCC_GPIOA);
	rcc_periph_clock_enable(RCC_USB);

	usbd_device* udev = usbd_init(
		&st_usbfs_v1_usb_driver, &dev, &config, usb_strings, 3, usbd_control_buffer, sizeof(usbd_control_buffer));

	usbd_register_set_config_callback(udev, cdcacm_set_config);

	xTaskCreate(usb_task, "USB", 300, udev, priority, NULL);
}

int usb_ready() {
	return initialized;
}