/* Simple LED task demo, using timed delays:
 *
 * The LED on PC13 is toggled in send_task.
 */
#define vPortSVCHandler SVC_Handler
#define xPortPendSVHandler PendSV_Handler
#define xPortSysTickHandler SysTick_Handler

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rtc.h>

#include <libopencm3/cm3/nvic.h>

#include "FreeRTOS.h"
#include "task.h"

#include "inc/main.h"
#include "inc/usbcdc.h"

__attribute((noreturn))
void vApplicationStackOverflowHook(xTaskHandle *pxTask __attribute((unused)),
  signed portCHAR *pcTaskName __attribute((unused))) {
	for(;;);	// Loop forever here..
}

__attribute((noreturn))
static void send_task(void *args __attribute((unused))) {
	for (;;) {
		gpio_toggle(GPIOC, GPIO13);
		usb_puts("lolgitgud!");
		vTaskDelay(pdMS_TO_TICKS(500));
	}
}

int main(void) {
	rcc_clock_setup_in_hse_8mhz_out_72mhz(); // For "blue pill"
	rtc_awake_from_off(RCC_HSE);
	rtc_set_prescale_val(62500);

	rcc_periph_clock_enable(RCC_GPIOC);
	gpio_set_mode(
		GPIOC,
		GPIO_MODE_OUTPUT_2_MHZ,
		GPIO_CNF_OUTPUT_PUSHPULL,
		GPIO13);

	gpio_set_mode(
		GPIOC,
		GPIO_MODE_OUTPUT_2_MHZ,
		GPIO_CNF_OUTPUT_PUSHPULL,
		GPIO14);
	gpio_clear(GPIOC, GPIO14);


//	xTaskCreate(send_task, "LED", 100, NULL, 1, NULL);

	usb_start(1);
	vTaskStartScheduler();

	for (;;);
}
