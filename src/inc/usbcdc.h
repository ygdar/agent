//
// Created by zombi on 10/22/19.
//

#ifndef AGENT_USBCDC_H
#define AGENT_USBCDC_H

#include <stdint.h>
#include <libopencm3/usb/usbd.h>

#define ENDPOINT_COMM_ADDR_IN USB_ENDPOINT_ADDR_IN(0x03)
#define ENDPOINT_DATA_ADDR_OUT USB_ENDPOINT_ADDR_OUT(0x01)
#define ENDPOINT_DATA_ADDR_IN USB_ENDPOINT_ADDR_IN(0x02)


static enum usbd_request_return_codes cdcacm_control_request(
	usbd_device* udev,
	struct usb_setup_data* req,
	uint8_t** buf,
	uint16_t* len,
	void (** complete) (usbd_device* udev, struct usb_setup_data* req)
);
static void cdcacm_data_rx_cb(usbd_device* udev, uint8_t ep);
static void cdcacm_set_config(usbd_device* udev, uint16_t value);

void usb_putc(uint8_t ch);
void usb_puts(const uint8_t* buf);

void usb_start(uint32_t priority);
int usb_ready();

#endif //AGENT_USBCDC_H
