#!/usr/bin/env python3

import os
import sys
import subprocess

TARGET_NAME = 'agent'

STFLASH = 'st-flash'

CURRENT_DIR = os.getcwd()
BUILD_DIR = os.path.join(CURRENT_DIR, 'build')

def build(build_type='Release'):
    toolchain_path = os.path.join(CURRENT_DIR, 'CMake/toolchain.cmake')

    if not os.path.isfile(toolchain_path):
        raise FileNotFoundError(toolchain_path)

    gen_command = ['cmake', f'-H{CURRENT_DIR}', f'-B{BUILD_DIR}', f'-DCMAKE_TOOLCHAIN_FILE={toolchain_path}', f'-DCMAKE_BUILD_TYPE={build_type}']
    gen_completed = subprocess.run(gen_command, capture_output=True)
    print(gen_completed.stdout.decode('utf-8'))

    build_command = ['make', f'-C{BUILD_DIR}']
    build_completed = subprocess.run(build_command, capture_output=True)
    print(build_completed.stdout.decode('utf-8'))

def flash():
    hex_path = os.path.join(BUILD_DIR, 'src', f'{TARGET_NAME}.hex')
    if not os.path.isfile(hex_path):
        build()
        if not os.path.isfile(hex_path):
            raise FileNotFoundError(hex_path)

    flash_command = f'{STFLASH} --format ihex write {hex_path}'
    print(flash_command)
    flash_completed = subprocess.run(flash_command, capture_output=True, shell=True)
    print(flash_completed.stdout.decode('utf-8'))

if __name__ == '__main__':
    op = sys.argv[1]
    if op == 'build':
        build()
    elif op == 'build:debug':
        build('Debug')
    elif op == 'flash':
        flash()
    else:
        print(f'op "{op}" not found')
